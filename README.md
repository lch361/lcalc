### Lcalc - command line calculator

This is a simple calculator with basic operations, math functions, and even base conversion with bit operations.
Work of this program is inspired by GNU bc. Unlike bc, you cannot define yout own functions, only calculate expressions.

## Build

- clone repository and cd into it
- `make debug | release`

## Install

- `sudo make install`
