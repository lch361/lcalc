#pragma once
#include <stdint.h>
#include "lcalc.h"

typedef struct {
	CalcMode mode;
	uint8_t float_precision;
	uint8_t base;
} CalcArgs;

/* Parses arguments.
 * Can exit the program by calling help message or getting an error.
 */
extern CalcArgs parse_args(int argc, const char **argv);
