#pragma once
#include <stdint.h>

typedef struct {
	uint8_t modes;
	const char *str;
	int64_t (*op_int)    (int64_t);
	double  (*op_double) (double);
} UnaryOperatorData;

typedef struct {
	uint8_t modes, priority;
	const char *str;
	int64_t (*op_int)    (int64_t, int64_t);
	double  (*op_double) (double, double);
} BinaryOperatorData;

typedef struct {
	uint8_t modes, args;
	const char *str;
	union {
		int64_t (*op_int_1) (int64_t);
		int64_t (*op_int_2) (int64_t, int64_t);
	};
	union {
		double (*op_double_1) (double);
		double (*op_double_2) (double, double);
	};
} FunctionData;

extern const UnaryOperatorData unary_ops[];
extern const BinaryOperatorData binary_ops[];
extern const FunctionData functions[];

extern const uint8_t unary_ops_len, binary_ops_len, functions_len;
