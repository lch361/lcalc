#pragma once
#include <stdint.h>

#define ERROR_BUFFER_LENGTH 100

extern char error_message[ERROR_BUFFER_LENGTH];

typedef enum { BASIC = 1, SCIENTIFIC = 2, PROGRAMMER = 4 } CalcMode;

/* Calculates a mathematical expression, stores result in *output.
 * *output type is int64 or double, depending on mode.
 * Return value is 0 or error code.
 */
extern int calculate(const char *expr, void *output, CalcMode mode);
