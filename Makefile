CC := cc

SOURCES := src
HEADERS := headers
TESTS   := tests
TARGET  := target
TEST    := $(TARGET)/test
RELEASE := $(TARGET)/release
DEBUG   := $(TARGET)/debug

CFLAGS := -std=c11 -Wall -Wextra -I$(HEADERS)
CFLAGS_DEBUG   := $(CFLAGS) -g -O0 -DDEBUG_BUILD
CFLAGS_RELEASE := $(CFLAGS) -Werror -O2 -march=native

LDFLAGS := -lm

LDFLAGS_DEBUG   := $(LDFLAGS)
LDFLAGS_RELEASE := $(LDFLAGS)

BIN := $(notdir $(shell pwd))

SRCS := $(shell ls $(SOURCES))
OBJS := $(SRCS:.c=.o)
SRCS := $(addprefix $(SOURCES)/, $(SRCS))

DEBUG_OBJS   := $(addprefix $(DEBUG)/,   $(OBJS))
RELEASE_OBJS := $(addprefix $(RELEASE)/, $(OBJS))

TEST_SRCS := $(shell ls $(TESTS))
TEST_BINS := $(addprefix $(TEST)/, $(TEST_SRCS:.c=))
TEST_SRCS := $(addprefix $(TESTS)/, $(TEST_SRCS))

PREFIX := /usr/local

debug: $(DEBUG)/$(BIN)

$(DEBUG):
	mkdir -p $@

$(DEBUG)/%.o: $(SOURCES)/%.c | $(DEBUG)
	$(CC) $(CFLAGS_DEBUG) -c -o $@ $<

$(DEBUG)/$(BIN): $(DEBUG_OBJS)
	$(CC) $(CFLAGS_DEBUG) -o $@ $^ $(LDFLAGS_DEBUG)

release: $(RELEASE)/$(BIN)

$(RELEASE):
	mkdir -p $@

$(RELEASE)/%.o: $(SOURCES)/%.c | $(RELEASE)
	$(CC) $(CFLAGS_RELEASE) -c -o $@ $<

$(RELEASE)/$(BIN): $(RELEASE_OBJS)
	$(CC) $(CFLAGS_RELEASE) -o $@ $^ $(LDFLAGS_RELEASE)

test: $(TEST_BINS)

$(TEST):
	mkdir -p $@

$(TEST)/%: $(TESTS)/%.c $(DEBUG_OBJS:$(DEBUG)/main.o=) | $(TEST)
	$(CC) $(CFLAGS_DEBUG) -o $@ $^ $(LDFLAGS_DEBUG)

options:
	@echo "CC = $(CC)"
	@echo ""
	@echo "CFLAGS         = $(CFLAGS)"
	@echo "CFLAGS_DEBUG   = $(CFLAGS_DEBUG)"
	@echo "CFLAGS_RELEASE = $(CFLAGS_RELEASE)"
	@echo ""
	@echo "LDFLAGS         = $(LDFLAGS)"
	@echo "LDFLAGS_DEBUG   = $(LDFLAGS_DEBUG)"
	@echo "LDFLAGS_RELEASE = $(LDFLAGS_RELEASE)"

install: release
	install -m 755 $(RELEASE)/$(BIN) $(PREFIX)/bin

uninstall:
	rm -f $(PREFIX)/bin/$(BIN)

all: debug release test

.ccls: Makefile
	echo "$(CC)\n$(shell echo "$(CFLAGS)" | sed 's| |\\n|g')" > $@

clean:
	rm -rf $(TARGET)

.PHONY: all clean options debug release test install uninstall
