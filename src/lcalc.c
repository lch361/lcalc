#include "lcalc.h"
#include "operators.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define STACK_SIZE 2048

typedef _Bool bool;
#define false 0
#define true  1

char error_message[ERROR_BUFFER_LENGTH] = {};

typedef enum {
	NUMBER = 32, UNARY_OP = 16, BINARY_OP = 8, FUNCTION = 4, CLOSE_FUNCTION = 2,
	COMMA = 1
} TokenTypes;

typedef struct {
	union { int64_t i; double d; };
} Number;

typedef struct {
	TokenTypes op_type;   // Is unary, binary or function
	uint8_t op_id;        // Is index to Data array
	uint8_t priority;
	uint8_t number_start; // Is index of first argument
} Operation;

int try_get_number(const char *start, char **end, CalcMode mode, Number *out) {
	int base = 10;
	switch (mode) {
		case SCIENTIFIC:
			out->d = strtod(start, end);
			return start == *end;
		case PROGRAMMER:
			if (start[0] == '0') {
				switch (start[1]) {
					case 'x': base = 16; start += 2; break;
					case 'b': base = 2;  start += 2; break;
					case 'o': base = 8;  start += 2; break;
				}
			}
			// fallthrough
		case BASIC:
			out->i = strtoll(start, end, base);
			return start == *end;
	}
	return -1;
}

int try_get_unary(const char *start, const char **end, CalcMode mode,
		Operation *out) {
	for (uint8_t i = 0; i < unary_ops_len; ++i) {
		uint8_t len = strlen(unary_ops[i].str);
		if (!(unary_ops[i].modes & mode &&
			strncmp(start, unary_ops[i].str, len) == 0)) {
			continue;
		}

		out->op_id = i;
		out->op_type = UNARY_OP;
		out->priority = 42;
		*end = start + len;
		return 0;
	}

	return -1;
}

int try_get_binary(const char *start, const char **end, CalcMode mode,
		Operation *out) {
	for (uint8_t i = 0; i < binary_ops_len; ++i) {
		uint8_t len = strlen(binary_ops[i].str);
		if (!(binary_ops[i].modes & mode &&
			strncmp(start, binary_ops[i].str, len) == 0)) {
			continue;
		}

		out->op_id = i;
		out->op_type = BINARY_OP;
		out->priority = binary_ops[i].priority;
		*end = start + len;
		return 0;
	}

	return -1;
}

int try_get_function(const char *start, const char **end, CalcMode mode,
		Operation *out) {
	for (uint8_t i = 0; i < functions_len; ++i) {
		uint8_t len = strlen(functions[i].str);
		if (!(functions[i].modes & mode &&
			strncmp(start, functions[i].str, len) == 0)) {
			continue;
		}

		out->op_id = i;
		out->op_type = FUNCTION;
		out->priority = 69;
		*end = start + len;
		return 0;
	}

	return -1;
}

#define CALCULATE_ARG  1
#define CALCULATE_FUNC 2

void calculate_operation(Number *numbers, Operation *operations,
	uint8_t *numbers_size, uint8_t *operations_size, CalcMode mode, int8_t i) {
	switch (operations[i].op_type) {
		case UNARY_OP:
			switch (mode) {
				case SCIENTIFIC:
					numbers[operations[i].number_start].d =
						unary_ops[operations[i].op_id].op_double(
								numbers[operations[i].number_start].d);
					break;
				default:
					numbers[operations[i].number_start].i =
						unary_ops[operations[i].op_id].op_int(
								numbers[operations[i].number_start].i);
					break;
			}
			break;
		case BINARY_OP:
			switch (mode) {
				case SCIENTIFIC:
					numbers[operations[i].number_start].d =
						binary_ops[operations[i].op_id].op_double(
								numbers[operations[i].number_start].d,
								numbers[operations[i].number_start + 1].d
								);
					break;
				default:
					numbers[operations[i].number_start].i =
						binary_ops[operations[i].op_id].op_int(
								numbers[operations[i].number_start].i,
								numbers[operations[i].number_start + 1].i
								);
					break;
			}
			--(*numbers_size);
			break;
		default: break;
	}
	--(*operations_size);
}

void calculate_function(Number *numbers, Operation *operations,
	uint8_t *numbers_size, uint8_t *operations_size, CalcMode mode, int8_t i) {
	uint8_t args_got = *numbers_size - operations[i].number_start;
	if (functions[operations[i].op_id].args != args_got) {
		sprintf(error_message, "Function %s expected %u arguments, got %u.",
				functions[operations[i].op_id].str,
				functions[operations[i].op_id].args,
				args_got);
		return;
	}

	switch (mode) {
	case SCIENTIFIC:
		switch (functions[operations[i].op_id].args) {
		case 1:
			numbers[operations[i].number_start].d =
				functions[operations[i].op_id].op_double_1(
						numbers[operations[i].number_start].d);
			break;
		case 2:
			numbers[operations[i].number_start].d =
				functions[operations[i].op_id].op_double_2(
						numbers[operations[i].number_start].d,
						numbers[operations[i].number_start + 1].d);
			--(*numbers_size);
			break;
		}
		break;
	default:
		switch (functions[operations[i].op_id].args) {
		case 1:
			numbers[operations[i].number_start].i =
				functions[operations[i].op_id].op_int_1(
						numbers[operations[i].number_start].i);
			break;
		case 2:
			numbers[operations[i].number_start].i =
				functions[operations[i].op_id].op_int_2(
						numbers[operations[i].number_start].i,
						numbers[operations[i].number_start + 1].i);
			--(*numbers_size);
			break;
		}
	}
	--(*operations_size);
}

int calculate(const char *expr, void *output, CalcMode mode) {
	strcpy(error_message, "");
	uint8_t appropriate_types_mask = 0b110100, last_pos = 0;
	Number numbers[STACK_SIZE];
	Operation operations[STACK_SIZE];
	uint8_t numbers_size = 0, operations_size = 0;

	do {
		bool flag = false;
		uint8_t operate_function = 0b00;
		
		const char *endptr;
		int res;

		if (appropriate_types_mask & NUMBER) {
			res = try_get_number(expr, (char **)&endptr, mode,
					numbers + numbers_size);
			if (res == 0) {
				++numbers_size;
				appropriate_types_mask = 0b001011;
				flag = true;
			}
		}

		if (!flag && appropriate_types_mask & UNARY_OP) {
			res = try_get_unary(expr, &endptr, mode,
					operations + operations_size);
			if (res == 0) {
				operations[operations_size].number_start = numbers_size;
				++operations_size;
				appropriate_types_mask = 0b100100;
				flag = true;
			}
		}

		if (!flag && appropriate_types_mask & BINARY_OP) {
			res = try_get_binary(expr, &endptr, mode,
					operations + operations_size);
			if (res == 0) {
				operations[operations_size].number_start = numbers_size - 1;
				++operations_size;
				appropriate_types_mask = 0b110100;
				flag = true;
			}
		}

		if (!flag && appropriate_types_mask & FUNCTION) {
			res = try_get_function(expr, &endptr, mode,
					operations + operations_size);
			if (res == 0) {
				operations[operations_size].number_start = numbers_size;
				++operations_size;
				appropriate_types_mask = 0b110100;
				flag = true;
			}
		}

		if (!flag && appropriate_types_mask & CLOSE_FUNCTION) {
			if (*expr == ')') {
				endptr = expr + 1;
				appropriate_types_mask = 0b001011;
				operate_function = CALCULATE_ARG | CALCULATE_FUNC;
				flag = true;
			}
		}

		if (!flag && appropriate_types_mask & COMMA) {
			if (*expr == ',') {
				endptr = expr + 1;
				appropriate_types_mask = 0b110100;
				operate_function = CALCULATE_ARG;
				flag = true;
			}
		}

		if (!flag) {
			sprintf(error_message, "Error: invalid token at position %u.",
					last_pos);
			return -1;
		}

		last_pos += endptr - expr;
		expr = endptr;

		int8_t last_op_i = operations_size - 1;
		uint8_t last_op_priority = operations[last_op_i].priority;
		for (int8_t i = last_op_i - 1; i >= 0 &&
				operations[i].priority >= last_op_priority &&
				operations[i].op_type != FUNCTION; --i) {
			calculate_operation(numbers, operations, &numbers_size,
					&operations_size, mode, i);
			if (strcmp(error_message, "") != 0) return -1;
		}
		if (operations_size - 1 != last_op_i) {
			operations[operations_size - 1] = operations[last_op_i];
			operations[operations_size - 1].number_start = numbers_size - 1;
		}

		if (operate_function & CALCULATE_ARG) {
			for (int8_t i = operations_size - 1;
					operations[i].op_type != FUNCTION; --i) {
				if (i < 0) {
					strcpy(error_message, "Expected function before ')'.");
					return -1;
				}
				calculate_operation(numbers, operations, &numbers_size,
						&operations_size, mode, i);
				if (strcmp(error_message, "") != 0) return -1;
			}
		}

		if (operate_function & CALCULATE_FUNC) {
			calculate_function(numbers, operations, &numbers_size,
					&operations_size, mode, operations_size - 1);
			if (strcmp(error_message, "") != 0) return -1;
		}
	} while (*expr != '\0');

	for (int8_t i = operations_size - 1; i >= 0; --i) {
		if (operations[i].op_type == FUNCTION) {
			sprintf(error_message, "Unclosed function %s detected.",
					functions[operations[i].op_id].str);
			return -1;
		}
		calculate_operation(numbers, operations, &numbers_size,
				&operations_size, mode, i);
		if (strcmp(error_message, "") != 0) return -1;
	}
	
	if (mode == SCIENTIFIC) {
		*(double *)output = numbers[0].d;
	}
	else {
		*(int64_t *)output = numbers[0].i;
	}

	return 0;
}
