#include "operators.h"
#include "lcalc.h"

#include <string.h>
#include <math.h>

int64_t minus_int(int64_t a) { return -a; }
double  minus_dbl(double a)  { return -a; }
int64_t bit_not(int64_t a) { return ~a; }

int64_t add_int(int64_t a, int64_t b) { return a + b; }
double  add_dbl(double a,  double b)  { return a + b; }
int64_t sub_int(int64_t a, int64_t b) { return a - b; }
double  sub_dbl(double a,  double b)  { return a - b; }
int64_t mul_int(int64_t a, int64_t b) { return a * b; }
double  mul_dbl(double a,  double b)  { return a * b; }
int64_t div_int(int64_t a, int64_t b) {
	if (b == 0) {
		strcpy(error_message, "Integer division by 0 occured.");
		return -1;
	}
	return a / b;
}
double  div_dbl(double a,  double b)  { return a / b; }

int64_t mod(int64_t a, int64_t b) {
	if (b == 0) {
		strcpy(error_message, "Integer division by 0 occured.");
		return -1;
	}
	return a % b;
}

int64_t bit_and(int64_t a, int64_t b) { return a & b; }
int64_t  bit_or(int64_t a, int64_t b) { return a | b; }
int64_t bit_xor(int64_t a, int64_t b) { return a ^ b; }
int64_t bit_shift_r(int64_t a, int64_t b) { return a >> b; }
int64_t bit_shift_l(int64_t a, int64_t b) { return a << b; }

int64_t bracket_int(int64_t a) { return a; }
double  bracket_dbl(double a)  { return a; }
int64_t abs_int(int64_t a) { return a > 0 ? a : -a; }
double  abs_dbl(double a)  { return a > 0 ? a : -a; }
int64_t min_int(int64_t a, int64_t b) { return a < b ? a : b; }
double  min_dbl(double a,  double  b) { return a < b ? a : b; }
int64_t max_int(int64_t a, int64_t b) { return a > b ? a : b; }
double  max_dbl(double a,  double  b) { return a > b ? a : b; }

double ctg(double a) { return 1.0 / tan(a); }
double actg(double a) { return atan(1.0 / a); }

int64_t gcd(int64_t a, int64_t b) {
	int64_t rem;
	while (b != 0) {
		rem = a % b;
		a = b;
		b = rem;
	}
	return a;
}

int64_t lcm(int64_t a, int64_t b) {
	return a * b / gcd(a, b);
}

int64_t fac(int64_t a) {
	int64_t res = 1;
	for (int64_t i = 1; i <= abs_int(a); ++i) res *= i;
	return a < 0 ? -res : res;
}

double log_by(double a, double b) { return log(a) / log(b); }

const UnaryOperatorData unary_ops[] = {
	{ .modes = 0x7, .str = "-", .op_int = minus_int, .op_double = minus_dbl },
	{ .modes = 0x4, .str = "~", .op_int = bit_not },
};

const BinaryOperatorData binary_ops[] = {
	{ .modes = 0x7, .priority = 3, .str = "+", .op_int = add_int, .op_double = add_dbl },
	{ .modes = 0x7, .priority = 3, .str = "-", .op_int = sub_int, .op_double = sub_dbl },
	{ .modes = 0x7, .priority = 4, .str = "*", .op_int = mul_int, .op_double = mul_dbl },
	{ .modes = 0x7, .priority = 4, .str = "/", .op_int = div_int, .op_double = div_dbl },
	{ .modes = 0x5, .priority = 4, .str = "%", .op_int = mod },
	{ .modes = 0x4, .priority = 1, .str = "&", .op_int = bit_and },
	{ .modes = 0x4, .priority = 1, .str = "|", .op_int = bit_or },
	{ .modes = 0x4, .priority = 1, .str = "^", .op_int = bit_xor },
	{ .modes = 0x4, .priority = 2, .str = ">>", .op_int = bit_shift_r },
	{ .modes = 0x4, .priority = 2, .str = "<<", .op_int = bit_shift_l },
};

const FunctionData functions[] = {
	{ .modes = 0x7, .str = "(",     .args = 1, .op_int_1 = bracket_int, .op_double_1 = bracket_dbl },
	{ .modes = 0x7, .str = "abs(",  .args = 1, .op_int_1 = abs_int, .op_double_1 = abs_dbl },
	{ .modes = 0x7, .str = "min(",  .args = 2, .op_int_2 = min_int, .op_double_2 = min_dbl },
	{ .modes = 0x7, .str = "max(",  .args = 2, .op_int_2 = max_int, .op_double_2 = max_dbl },
	{ .modes = 0x5, .str = "gcd(",  .args = 2, .op_int_2 = gcd },
	{ .modes = 0x5, .str = "lcm(",  .args = 2, .op_int_2 = lcm },
	{ .modes = 0x5, .str = "fac(",  .args = 1, .op_int_1 = fac },
	{ .modes = 0x2, .str = "cos(",  .args = 1, .op_double_1 = cos },
	{ .modes = 0x2, .str = "acos(", .args = 1, .op_double_1 = acos },
	{ .modes = 0x2, .str = "sin(",  .args = 1, .op_double_1 = sin },
	{ .modes = 0x2, .str = "asin(", .args = 1, .op_double_1 = asin },
	{ .modes = 0x2, .str = "tg(",   .args = 1, .op_double_1 = tan },
	{ .modes = 0x2, .str = "atg(",  .args = 1, .op_double_1 = atan },
	{ .modes = 0x2, .str = "ctg(",  .args = 1, .op_double_1 = ctg },
	{ .modes = 0x2, .str = "actg(", .args = 1, .op_double_1 = actg },
	{ .modes = 0x2, .str = "pow(",  .args = 2, .op_double_2 = pow },
	{ .modes = 0x2, .str = "log(",  .args = 2, .op_double_2 = log_by },
};

const uint8_t unary_ops_len = 2, binary_ops_len = 10, functions_len = 16;
