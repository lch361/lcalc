#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "args.h"
#include "lcalc.h"

#define BUFFER_LENGTH 4096

int get_stripped_string(char *buf, size_t length) {
	char c;
	for (size_t i = 0; i < length; ) {
		switch (c = getchar()) {
		case EOF:
		case '\n':
			buf[i] = '\0';
			return 0;
		default:
			if (!isspace(c)) {
				buf[i] = c;
				++i;
			}
		}
	}

	while (getchar() != '\n' && !feof(stdin));
	fprintf(stderr, "Length of this strings exceeded %lu.\n", length - 1);
	return -1;
}

int main(int argc, const char **argv) {
	CalcArgs args = parse_args(argc - 1, argv + 1);

	char buffer[BUFFER_LENGTH];
	int last_result = 0;
	int64_t result_i;
	double *result_d = (double *)&result_i;

	while (!feof(stdin) && !ferror(stdin)) {
		last_result = 0;

		if ((last_result = get_stripped_string(buffer, BUFFER_LENGTH)) == -1) {
			continue;
		}

		if (strcmp(buffer, "") == 0 || strcmp(buffer, ":q") == 0) {
			break;
		}

		if ((last_result = calculate(buffer, &result_i, args.mode)) == -1) {
			fprintf(stderr, "%s\n", error_message);
			continue;
		}

		if (args.mode == SCIENTIFIC) {
			printf("%.*f\n", args.float_precision, *result_d);
		}
		else {
			printf("%li\n", result_i);
		}
	}

	return last_result;
}
