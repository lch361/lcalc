#include "args.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define PROG_NAME "lcalc"

void print_help_msg() {
	printf(
		"Usage:\n"
		PROG_NAME" -h: display this message.\n"
		PROG_NAME" -{s|p}: change calculating mode from basic to scientific | programming.\n"
		PROG_NAME" -f 3: set floating output precision to 3.\n"
	);
}

int read_uint8(const char *src, uint8_t *dest) {
	uint8_t result = 0;
	while (*src != '\0') {
		if (*src < '0' || '9' < *src) return -1;
		result = result * 10 + *src++ - '0';
	}
	*dest = result;
	return 0;
}

CalcArgs parse_args(int argc, const char **argv) {
	CalcArgs result = {
		.float_precision = 6,
		.mode = BASIC
	};

	for (int i = 0; i < argc; ++i) {
		if (strlen(argv[i]) != 2 || argv[i][0] != '-') {
			fprintf(stderr, "Unrecognised arg \"%s\".\n", argv[i]);
			fprintf(stderr, "Try \""PROG_NAME" --help\" instead.\n");
			exit(-1);
		}

		switch (argv[i][1]) {
		case 'h':
			print_help_msg();
			exit(0);
		case 's':
			result.mode = SCIENTIFIC;
			break;
		case 'p':
			result.mode = PROGRAMMER;
			break;
		case 'f':
			++i;
			if (i != argc &&
				read_uint8(argv[i], &result.float_precision) != -1) break;
			fprintf(stderr, "Not a uint8_t type: \"%s\".\n", argv[i]);
			exit(-1);
		default:
			fprintf(stderr, "Unrecognised arg \"%s\".\n", argv[i]);
			fprintf(stderr, "Try \""PROG_NAME" --help\" instead.\n");
			exit(-1);
		}
	}

	return result;
}
